import compile
import aurolib

prim_types = {
  "u8": "uint8_t",
  "u16": "uint16_t",
  "u32": "uint32_t",
  "u64": "uint64_t",
  "i8": "int8_t",
  "i16": "int16_t",
  "i32": "int32_t",
  "i64": "int64_t",
  "f32": "float",
  "f64": "double",
}

prim_items = {}

for k, v in prim_types.items():
    tp = aurolib.CType(v)
    prim_items[k] = tp

    if k[0] == "i" or k[0] == "u":
        base_type = aurolib.int_tp
    elif k[0] == "f":
        base_type = aurolib.float_tp

    prim_items["%s\x1dnew" % k] = aurolib.CMacro(tp, "(%s) #0" % tp)
    prim_items["%s\x1dget" % k] = aurolib.CMacro(base_type, "(%s) #0" % base_type)

#ptr_type = aurolib.modules["auro\x1fmachine\x1fmem"]["pointer"]

def get_ctype (tp):
    # TODO: All types are valid like this lmao
    if isinstance(tp, aurolib.CType):
        return tp.name

class CExternFunction:
    def __init__(self, name, in_types, out_type):
        self.name = name
        self.in_types = in_types
        self.out_type = out_type

    def compile(self):
        args = ", ".join(["%s _%d" % (type, i) for i, type in enumerate(self.in_types)])
        return "extern %s %s (%s);" % (self.out_type or "void", self.name, args)

    def use(self, args):
        return "%s(%s)" % (self.name, ", ".join(args))


class FfiItemModule:
    def __init__(self, argument=None):
        if argument == None:
            return

        self.intypes = []
        while True:
            i = len(self.intypes)
            try: tp = argument["in%d" % i]
            except Exception: break
            if not tp: break

            self.intypes.append(tp)

        try: self.outtype = argument["out"]
        except Exception: self.outtype = None

        name_item = argument["name"]

        if hasattr(name_item, "nugget") and name_item.nugget:
            nugget_fn = name_item.nugget
        elif hasattr(name_item, "get_nugget"):
            nugget_fn = name_item.get_nugget()
        else:
            raise Exception("ffi name function argument can't be run in nugget: %r" % name_item)

        results = nugget_fn.call([])

        # TODO: Typecheck here

        if len(results) != 1 or type(results[0]) != str:
            raise Exception("argument.0 for ffi.import should return a string")

        self.name = results[0]
        self.item = CExternFunction(self.name, self.intypes, self.outtype)

    
    def build(self, argument):
        return FfiItemModule(argument)

    def __getitem__(self, key):
        if hasattr(self, "item") and key == "":
            return self.item

        raise Exception("Key %r not found in ffi.import" % key)

class FfiImportModule:
    def __init__(self, argument=None):
        if argument == None:
            return

        name_item = argument["0"]

        if hasattr(name_item, "nugget") and name_item.nugget:
            nugget_fn = name_item.nugget
        elif hasattr(name_item, "get_nugget"):
            nugget_fn = name_item.get_nugget()
        else:
            raise Exception("ffi name function argument can't be run in nugget: %r" % name_item)

        results = nugget_fn.call([])

        # TODO: Typecheck here

        if len(results) != 1 or type(results[0]) != str:
            raise Exception("argument.0 for ffi.import should return a string")

        libname = results[0]
        if libname.startswith("lib") and not libname.endswith(".so"):
            compile.flags.append("-l%s" % libname[3:])
        else:
            compile.flags.append("-l:%s" % libname)
    
    def build(self, argument):
        return FfiImportModule(argument)

    def __getitem__(self, key):
        if key == "get":
            return FfiItemModule()


        raise Exception("Key %r not found in ffi.import" % key)

class FfiModule:
    def __getitem__(self, key):
        if key == "import":
            return FfiImportModule()



class FfiFunctionModule:
    def __init__(self, argument=None):
        if argument == None:
            return

        orig_fn = argument["function"]

        self.in_types = []
        while True:
            i = len(self.in_types)
            try: tp = argument["in%d" % i]
            except Exception: break
            if not tp: break

            self.in_types.append(tp)

        try: self.out_type = argument["out"]
        except Exception: self.out_type = None

        if hasattr(orig_fn, 'name') and orig_fn.name:
            self.item = aurolib.CMacro(aurolib.ptr_tp, "&" + orig_fn.name)

        else:
            name = "__auro_ffi_f_%d" % compile.gen_id();

            params = ", ".join(["%s _%d" % (t, i) for i, t in enumerate(self.in_types)])
            args = ["_%d" % i for i in range(0, len(self.in_types))]
            impl = "%s %s (%s) { %s%s; }\n" % (self.out_type or "void", name, params, "return " if self.out_type else "", orig_fn.use(args))

            compile.compiled += impl
            self.item = aurolib.CMacro(aurolib.ptr_tp, "&" + name)

    
    def build(self, argument):
        return FfiFunctionModule(argument)

    def __getitem__(self, key):
        if hasattr(self, "item") and key == "":
            return self.item

        raise Exception("Key %r not found in ffi.import" % key)