
import re
import parse
import nugget

class Output:
    def __init__(self):
        self.set = set()
        self.output = "int argc; char **argv;\n"

    def add(self, value):
        self.set.add(value)

    def write(self, text):
        self.output += text

    def __contains__(self, item):
        return item in self.set

    def __iadd__(self, other):
        self.write(other)
        return self

    def compile(self, item):
        if hasattr(item, "dependencies") and isinstance(item.dependencies, list):
            for dep in item.dependencies:
                self.compile(dep)

        if hasattr(item, "compile"):
            if not (item in self.set):
                self.set.add(item)
                text = item.compile()

                if text != None:
                    self.output += text
                    if text[-1] != "\n":
                        self.output += "\n"


modules = []
flags = []
includes = set(["stdlib", "stdio", "string"])
compiled = Output()
const_calls = []

name_ids = {}
def gen_id(key="__empty__"):
    if key in name_ids:
        name_ids[key] += 1
    else:
        name_ids[key] = 1
    return name_ids[key]
def gen_name(base=""):
    base = re.sub(r'^(\d)', r'_\1', base)
    base = re.sub(r'([^\w]+)', r'_', base)
    return "auro_%s_%d" % (base, gen_id(base))
def get_nugget(obj):
    if hasattr(obj, 'nugget'):
        return obj.nugget
    if hasattr(obj, 'get_nugget'):
        return obj.get_nugget()

import aurolib

class Name:
    def __init__(self, name):
        self.parts = name.split("\x1d")
        self.main = self.parts[0]
        del self.parts[0]
    def match (self, q):
        if self.main != q.main:
            return False
        if len(self.parts) < len(q.parts):
            return False
        for p in q.parts:
            if p in self.parts:
                self.parts.remove(p)
            else:
                return False
        return True

def find_item (items, key):
    if key in items: return items[key]

    matches = []
    name = Name(key)
    for k, v in items.items():
        iname = Name(k)
        if iname.match(name):
            if len(iname.parts) == 0:
                return v
            matches.append((k, v))

    if len(matches) == 0:
        raise Exception("%s not found" % key)
    elif len(matches) > 1:
        raise Exception("Multiple matches for %s" % key)
    return matches[0][1]

class Lbl:
    def __init__(self, name, index):
        self.name = name
        self.index = index

class TypeAssign:
    def __init__(self, dst, src, pos):
        self.src = src
        self.dst = dst
        self.pos = pos
    def __repr__(self):
        return "TypeAssign(src=%r,dst=%r,pos=%d)" % (self.src, self.dst, self.pos)

class Code:
    def __init__(self, parsed, module):
        self.parsed = parsed
        self.module = module

        self.name = gen_name("fn")
        
    def compile (self):
        global compiled

        module = self.module

        def compile_fn(fn): return module.compile_fn(fn)
        def compile_tp(tp): return module.compile_tp(tp)

        self.parsed.compiled = self
        parsed = module.parsed

        outcount = len(self.parsed.outs)

        regs = []
        reg_types = []

        def add_reg(name, tp):
            regs.append(name)
            reg_types.append(tp)

        string = ""

        # self because python for some stupid reason cannot mutate it otherwise
        self.vari = 0
        self.lbli = 0
        def genvar ():
            self.vari += 1
            return "var_%r" % self.vari
        def genlbl ():
            self.lbli += 1
            return "lbl_%r" % self.lbli

        # Declare out type

        if outcount > 1:
            self.out_type = gen_name("type")
            compiled += "struct %s;\n" % self.out_type
            compiled += "typedef struct %s %s;\n" % (self.out_type, self.out_type)
        else:
            self.out_type = None

        # Compile types
        hargs = []
        self.in_types = []

        for tpi in self.parsed.ins:
            tp = compile_tp(tpi)
            var = genvar()
            add_reg(var, tp)
            hargs.append("%s %s" % (tp, var))
            self.in_types.append(tp)

        if outcount == 1:
            self.out_type = compile_tp(self.parsed.outs[0])
        elif outcount > 1:
            self.out_types = [compile_tp(tp) for tp in self.parsed.outs]
            compiled += "struct %s {\n" % self.out_type
            for i, tp in enumerate(self.out_types):
                compiled += "\t%s _%d;\n" % (tp, i)
            compiled += "};\n"

        # Declare function
        header = "%s %s (%s)" % (self.out_type or "void", self.name, ", ".join(hargs))
        compiled += header + ";\n"
        
        # Define and then print

        stmts = []
        lbls = []
        type_sets = []

        for inst in self.parsed.code:
            stmt = None
            if isinstance(inst, parse.CallInst):
                fn = compile_fn(inst.fn)

                stmt = fn.use([regs[n] for n in inst.ins])
                
                rcount = len(inst.outs)
                if rcount > 0:
                    var = genvar()
                    stmt = "%s %s = %s" % (fn.out_type, var, stmt)
                    if rcount > 1:
                        for i in range(0, rcount):
                            add_reg("%s._%d" % (var, i), fn.out_types[i])
                    else:
                        add_reg(var, fn.out_type)
                stmt
            elif isinstance(inst, parse.EndInst):
                if len(inst.args) == 0:
                    stmt = "return"
                elif len(inst.args) == 1:
                    stmt = "return %s" % regs[inst.args[0]]
                else:
                    rets = [regs[a] for a in inst.args]
                    stmt = "return (%s){%s}" % (self.out_type, ", ".join(rets))
            elif isinstance(inst, parse.DupInst):
                var = genvar()
                tp = reg_types[inst.a]
                if tp:
                    stmt = "%s %s = %s" % (tp, var, regs[inst.a])
                    add_reg(var, tp)
                else:
                    # Type of the value is not yet known
                    type_sets.append(TypeAssign(
                        dst=len(regs), src=inst.a, pos=len(stmts)
                    ))
                    stmt = "%s = %s" % (var, regs[inst.a])
                    add_reg(var, False)
            elif isinstance(inst, parse.VarInst):
                var = genvar()
                stmt = False 
                add_reg(var, False)
            elif isinstance(inst, parse.SetInst):
                src = inst.a
                dst = inst.b
                if not reg_types[dst]:
                    type_sets.append(TypeAssign(
                        dst=dst, src=src, pos=len(stmts)
                    ))
                stmt = "%s = %s" % (regs[dst], regs[src])
            elif isinstance(inst, parse.JmpInst):
                lbl = Lbl(genlbl(), inst.i)
                lbls.append(lbl)
                stmt = "goto %s" % lbl.name
            elif isinstance(inst, parse.JifInst):
                lbl = Lbl(genlbl(), inst.i)
                lbls.append(lbl)
                stmt = "if (%s) goto %s" % (regs[inst.a], lbl.name)
            elif isinstance(inst, parse.NifInst):
                lbl = Lbl(genlbl(), inst.i)
                lbls.append(lbl)
                stmt = "if (!%s) goto %s" % (regs[inst.a], lbl.name)
            else:
                raise Exception("%r not supported" % inst)
            stmts.append(stmt)

        lbls.sort(key=lambda lbl: lbl.index)
        for lbl in reversed(lbls):
            stmts.insert(lbl.index, "%s:" % lbl.name)

        decls = []
        while len(type_sets) > 0:
            newlist = []
            for decl in type_sets:
                if not reg_types[decl.dst]:
                    tp = reg_types[decl.src]
                    if tp:
                        decls.append("%s %s" % (tp, regs[decl.dst]))
                        reg_types[decl.dst] = tp
                    else:
                        newlist.append(decl)
            if len(newlist) == len(type_sets):
                raise Exception("Variable assignment loop, in %s" % self.sourcename)
            type_sets = newlist

        string = header + " {\n"
        string += "\t%s;\n" % "; ".join(decls)
        for stmt in stmts:
            if stmt:
                string += "\t%s;\n" % stmt
        string += "}\n"

        compiled += string

    def use(self, args):
        return "%s(%s)" % (self.name, ", ".join(args))

class CallConstant:
    def __init__ (self, parsd, index, mod):
        global compiled
        self.name = gen_name("const")

        nmod = get_nugget(mod)
        self.nugget = nugget.InternalFunction(nmod, index)

        results = self.nugget.call([])

        val = results[0]

        base_parsed = mod.parsed.functions[parsd.call.fn_index]
        self.out_type = mod.compile_tp(base_parsed.outs[0])

        if self.out_type == aurolib.int_tp:
            self.expr = str(val)
            self.out_type = aurolib.int_tp
        elif self.out_type == aurolib.str_tp:
            self.expr = '"%s"' % val.replace('\\', "\\\\").replace('"', "\\\"").replace('\n', "\\n")
            self.out_type = aurolib.str_tp
        elif self.out_type == aurolib.float_tp:
            self.expr = str(val)
            self.out_type = aurolib.float_tp
        else:
            raise Exception("Nugget functions not yet supported")

        '''
        findex = parsd.call.fn_index
        fn = mod.parsed.functions[findex]

        self.out_type = mod.compile_tp(fn.outs[0])

        if len(fn.outs) != 1:
            raise Exception("Multiple result constant calls not yet supported")

        fn = mod.compile_fn(findex)

        nugget_fn = get_nugget(fn)
        print("%s nugget -> %r" % (fn, nugget_fn))
        if nugget_fn != None:
            results = nugget_fn.call([])
            raise Exception("Nugget functions not yet supported")

        else:
            args = [mod.compile_fn(n) for n in parsd.call.args]
            const_calls.append("%s = %s" % (self.name, fn.use([fn.use([]) for fn in args])))

            compiled += "%s %s;\n" % (self.out_type, self.name)
            self.out_type = fn.out_type
        '''

    def use(self, args):
        if hasattr(self, 'expr'):
            return self.expr
        return self.name

class BufferConstant:
    def __init__(self, cns):
        self.name = gen_name("const")
        self.out_type = "auro_buffer"
        vals = [str(int(b)) for b in cns.bytes]
        self.compiled = "auro_buffer %s = { %d, (char[]){%s} };\n" % (self.name, len(vals), ",".join(vals))
    def compile (self):
        buf_mod = get_module("auro\x1fbuffer")
        buf_type = buf_mod["buffer"]
        compiled.compile(buf_type)

        return self.compiled
    def use(self, args):
        return self.name

class IntConstant:
    def __init__(self, fn):
        self.value = str(fn.value)
        self.out_type = "int"
    def use(self, args):
        return self.value

class DefineModule:
    def __init__(self, base, mod):
        self.base = base
        self.items = mod.items
    def __getitem__(self, key):
        item = find_item(self.items, key)
        if item.kind == 0:
            return self.base.get_module(item.index)
        elif item.kind == 1:
            return self.base.compile_tp(item.index)
        elif item.kind == 2:
            return self.base.compile_fn(item.index)
        else:
            raise Exception("Invalid module item kind %d" % item.kind)

class SourceMap:
    def __init__(self, node):
        self.file = node["file"][0]
        self.functions = {}

        for node in node:
            if node[0] == "function":
                self.functions[node[1].value] = node

class ParsedModule:
    def __init__(self, name, parsed, argument=None):
        self.name = name
        self.parsed = parsed
        self.functions = {}
        self.types = {}
        self.modules = {}
        self.argument = argument

        sourcemap = parsed.metadata["source map"]
        if sourcemap: self.sourcemap = SourceMap(sourcemap)

    def get_module(self, index):
        if index == 0:
            if self.argument:
                return self.argument
            else:
                raise Exception("Module %r was not instantiated with an argument" % self.name)
        if index in self.modules:
            return self.modules[index]

        parsed = self.parsed.modules[index-1]

        if isinstance(parsed, parse.ImportModule):
            mod = get_module(parsed.name)
        elif isinstance(parsed, parse.BuildModule):
            base = self.get_module(parsed.base)
            arg = self.get_module(parsed.argument)
            mod = base.build(arg)
        elif isinstance(parsed, parse.DefineModule):
            mod = DefineModule(self, parsed)
        elif isinstance(parsed, parse.UseModule):
            base = self.get_module(parsed.base)
            mod = base[parsed.name]
        else:
            raise Exception("Module %r not yet supported" % parsed)

        self.modules[index] = mod
        compiled.compile(mod)
        return mod

    def compile_fn(self, index):

        if index in self.functions:
            return self.functions[index]

        fn = self.parsed.functions[index]

        if isinstance(fn, parse.CodeFunction):
            fn = Code(fn, self)
            self.functions[index] = fn

            if self.sourcemap:
                meta = self.sourcemap.functions[index]
                if meta:
                    fn.sourcename = meta["name"][1].value
                    fn.name = gen_name(fn.sourcename)

        elif isinstance(fn, parse.CallConstant):
            fn = CallConstant(fn, index, self)
        elif isinstance(fn, parse.ImportFunction):
            mod = self.get_module(fn.module)
            fn = mod[fn.name]
        elif isinstance(fn, parse.BufferConstant):
            fn = BufferConstant(fn)
        elif isinstance(fn, parse.IntConstant):
            fn = IntConstant(fn)
        else:
            raise Exception("Unsupported function: %r" % fn)

        self.functions[index] = fn
        compiled.compile(fn)
        return fn

    def compile_tp(self, tp):
        tp = self.parsed.types[tp]
        module = self.get_module(tp.module)
        it = module[tp.name]
        compiled.compile(it)
        return it

    def build(self, argument):
        # TODO; Cache
        return ParsedModule(self.name, self.parsed, argument)

    def get_nugget(self):
        return nugget.NuggetModule.from_file(self.parsed.filename)

    def __getitem__ (self, key):
        return self.get_module(1)[key]

    def __repr__(self):
        return "ParsedModule(%r)" % self.name

def set_module_loader (loader):
    global module_loader
    module_loader = loader

def get_module (name):
    mod = module_loader(name)
    if not mod:
        try: mod = aurolib.modules[name]
        except Exception:
            raise Exception("module '%s' not found" % name)
    return mod

def compile(modname):
    global compiled

    module = get_module(modname)
    main = module["main"]

    compiled += "void main (int _argc, char **_argv) {\n"
    compiled += "\targc = _argc; argv = _argv;\n"
    for c in const_calls:
        compiled += "\t%s;\n" % c
    compiled += "\t%s;\n}" % main.use([])


    include_line = "".join(["#include <%s.h>\n" % s for s in includes])
    compiled.output = include_line + compiled.output

    return compiled

