
import compile
import nugget

class CModule:
    def __init__(self, items={}, comp=None):
        self.items = items
        self.inner_compile = comp
        self.initialized = False

    def compile (self):
        if self.inner_compile != None:
            return self.inner_compile()
        return None

    def __getitem__(self, key):
        compile.compiled.compile(self)
        if key in self.items:
            item = self.items[key]
            compile.compiled.compile(item)
            return item
        else:
            raise Exception("Item %r not found in %r" % (key, self.name))

class CType:
    def __init__(self, name, definition = None):
        self.name = name
        self.id = compile.gen_id()
        if name == None:
            self.name = "__t_%d" % self.id

        self.definition = definition

    def __str__(self):
        return self.name

    def __repr__(self):
        return "CType(%s)" % self.name

    def compile (self):
        return self.definition

class CPtrType (CType):
    def __init__(self, name, definition = None):
        CType.__init__(self, "%s*" % name, definition)


class CFunction:
    def __init__(self, out_type, name, body, deps=None):
        self.out_type = out_type
        if isinstance(out_type, list):
            self.out_types = out_type

            fields = " ".join(["%s _%s;" % (x, i) for i, x in enumerate(out_type)])
            full_type = "struct { %s }" % fields
            self.out_type = CType(full_type)
        self.name = name
        self.body = body
        self.dependencies = deps

    def compile(self):
        return "%s %s %s" % (self.out_type or "void", self.name, self.body)

    def use(self, args):
        return "%s(%s)" % (self.name, ", ".join(args))

class CMacro:
    def __init__(self, out_type, body):
        self.out_type = out_type
        self.body = body

    def use(self, args):
        s = self.body
        for i in range(0, len(args)):
            s = s.replace("#%i"%i, args[i])
        return s

class GenericModule:
    def __init__(self, argument=None):
        self.argument = argument
        if argument:
            self.init(argument)
        else:
            self.instances = {}
            self.init_base()

    def init(self, argument): pass
    def init_base(self): pass

    def get_key(argument):
        return argument["0"].id
    
    def build(self, argument):
        Self = type(self)
        key = Self.get_key(argument)
        if not (key in self.instances):
            self.instances[key] = Self(argument)
        return self.instances[key]

    def __getitem__(self, key):
        if hasattr(self, 'get'):
            item = self.get(key)
        elif hasattr(self, 'items') and key in self.items:
            item = self.items[key]
        else:
            raise Exception("Key %r not found in %s" % (key, type(self).name))

        compile.compiled.compile(item)
        return item


class RecordModule(GenericModule):
    name = "record"
    def init(self, argument):
        self.types = []
        self.items = {}

        self.pure_type = compile.gen_name("record")
        self.type = CPtrType(self.pure_type)

        self.items[""] = self.type
        while True:
            i = len(self.types)
            try: type = argument[str(i)]
            except Exception: break
            if not type: break

            self.types.append(type)
            self.items["get%d"%i] = CMacro(type, "#0->_%d"%i)
            self.items["set%d"%i] = CMacro(None, "#0->_%d = #1"%i)

        # New function
        fn_args = ", ".join(["%s _%d" % (tp, i) for i, tp in enumerate(self.types)])
        val_body = ", ".join(["_%d"%i for i in range(0, len(self.types))])
        self.items["new"] = CFunction(self.type, "auro_new_record_%s" % self.type.id,
            "(%s) {" % fn_args +
            "\n\t%s *value = malloc(sizeof(%s));" % (self.pure_type, self.pure_type) +
            "\n\t*value = (%s) { %s };" % (self.pure_type, val_body) +
            "\n\treturn value;" +
            "\n}"
        )

    def compile(self):
        if not hasattr(self, "types"):
            return None

        output = "typedef struct {\n"
        for i, type in enumerate(self.types):
            output += "\t%s _%d;\n" % (type, i)
        output += "} %s;\n" % self.pure_type
        return output

    def get_key(argument):
        ids = []
        while True:
            i = len(ids)
            try: type = argument[str(i)]
            except Exception: break
            if not type: break
            ids.append(str(type.id))
        return ",".join(ids)

class ArrayModule(GenericModule):
    name = "array"
    def init (self, argument):
        self.base = argument["0"]

        self.raw = compile.gen_name("array")
        self.tp = CPtrType(self.raw)

        self.items = {
            "": self.tp,
            "get": CMacro(self.base, "#0->vals[#1]"),
            "set": CMacro(None, "#0->vals[#1] = #2"),
            "len": CMacro(int_tp, "#0->len"),
            "new": CFunction(self.tp, "auro_new_array_%s" % self.tp.id,
                "(%s value, int len) {" % self.base +
                "\n\t%s arr = malloc(sizeof(%s));" % (self.tp, self.raw) +
                "\n\tarr->len = len;" +
                "\n\tarr->vals = malloc(sizeof(%s)*len);" % self.base +
                "\n\tfor (int i=0;i<len;i++) arr->vals[i] = value;"
                "\n\treturn arr;" +
                "\n}"
            ),
            "empty": CFunction(self.tp, "auro_empty_array_%s" % self.tp.id,
                "() {" +
                "\n\t%s arr = malloc(sizeof(%s));" % (self.tp, self.raw) +
                "\n\tarr->len = 0;" +
                "\n\tarr->vals = 0;" +
                "\n\treturn arr;" +
                "\n}"
            ),
        }

    def compile (self):
        if not hasattr(self, 'base'):
            return None

        return "\n".join([
            "typedef struct {",
            "\t%s *vals;" % self.base,
            "\tint len;",
            "} %s;" % self.raw,
        ])


# Compile any type
any_tp = CType("auro_any")
compile.compiled += "typedef struct { int type; union { void* ptr; int n; float f; char c; } value; } auro_any;\n"

class AnyModule (GenericModule):
    name = "any"

    def init(self, argument):
        tp = argument["0"]
        self.base = tp

        test = CMacro(bool_tp, "#0.type == %d" % tp.id)

        variant = None

        if tp == int_tp or tp == bool_tp:
            variant = "n"
        elif tp == float_tp:
            variant = "f"
        elif tp == char_tp:
            variant = "c"
        elif isinstance(tp, CPtrType):
            variant = "ptr"

        if variant:
            self.items = {
                "new": CMacro(any_tp, "(auro_any) { %d, { .%s = #0 } }" % (tp.id, variant)),
                "get": CMacro(tp, "#0.value.%s" % variant),
                "test": test,
            }
        else:
            self.items = {
                "new": CFunction(any_tp, "auro_any_new_%d" % tp.id,
                    "(%s value) {" % tp +
                    "\n\t%s *ptr = malloc(sizeof(%s));" % (tp, tp) +
                    "\n\t*ptr = value;" +
                    "\n\treturn (auro_any) { %d, { .ptr=ptr } };" % tp.id +
                    "\n}"
                ),
                "get": CMacro(tp, "*(%s*)#0.value.ptr" % tp),
                "test": test,
            }

    def init_base(self):
        self.items = {"any": any_tp}

class TypeShell:
    def __init__(self, arg=None):
        self.arg = arg
        self.items = {}
        if arg:
            self.tp = CPtrType("void")
            self.items[""] = self.tp

    def build(self, argument):
        return TypeShell(argument)

    def __getitem__(self, key):
        if key in self.items:
            return self.items[key]
        elif key == "new":
            base = self.arg["0"]
            if isinstance(base, CPtrType):
                item = CMacro(self.tp, "#0")
            else:
                item = CFunction(self.tp, "auro_shell_new_%d" % self.tp.id,
                    "(%s value) {" % base +
                    "\n\t%s *ptr = malloc(sizeof(%s));" % (base, base) +
                    "\n\t*ptr = value;" +
                    "\n\treturn ptr;" +
                    "\n}"
                )
        elif key == "get":
            base = self.arg["0"]
            if isinstance(base, CPtrType):
                item = CMacro(base, "#0")
            else:
                item = CMacro(base, "*(%s*)#0" % base)
        else:
            raise Exception("key %r not found in typeshell" % key)

        self.items[key] = item
        compile.compiled.compile(item)
        return item

class NullType (CPtrType):
    def __init__(self, base):
        self.allocated = isinstance(base, NullType) or not isinstance(base, CPtrType)
        name = str(base)
        if self.allocated:
            CPtrType.__init__(self, name)
        else:
            CType.__init__(self, name)


class NullModule (GenericModule):
    name = "null"

    def init(self, argument):
        tp = argument["0"]
        self.base = tp
        self.tp = NullType(tp)

        if self.tp.allocated:
            self.items = {
                "new": CFunction(self.tp, "auro_null_new_%d" % tp.id,
                    "(%s value) {" % tp +
                    "\n\t%s *ptr = malloc(sizeof(%s));" % (tp, tp) +
                    "\n\t*ptr = value;" +
                    "\n\treturn ptr;" +
                    "\n}"
                ),
                "get": CMacro(tp, "*#0"),
            }
        else:
            self.items = {
                "new": CMacro(self.tp, "#0"),
                "get": CMacro(tp, "#0"),
            }

        self.items.update({
            "": self.tp,
            "null": CMacro(self.tp, "0"),
            "isnull": CMacro(bool_tp, "#0 == 0"),
        })


def get_fn_params (argument):
    ins = []
    outs = []

    while True:
        i = len(ins)
        try: type = argument["in%d"%i]
        except Exception: break
        if not type: break
        ins.append(type)
    while True:
        i = len(outs)
        try: type = argument["out%d"%i]
        except Exception: break
        if not type: break
        outs.append(type)
    return ins, outs

class FnValue:
    def __init__(self, name, tp):
        self.name = name
        self.out_type = tp
    def use(self, args):
        return self.name

class NewFnModule:
    def __init__(self, base, argument):
        fn = argument["0"]

        self.base = base
        name = compile.gen_name("fnval")
        self.items = { "": FnValue(name, self.base.type) }

        args = ["_%s"%i for i in range(len(self.base.ins))]

        params = ["%s _%d" % (tp, i) for i, tp in enumerate(self.base.ins)]
        params.insert(0, "void *_")

        body = fn.use(args)
        if len(self.base.outs) > 0:
            body = "return " + body

        self.compiled = "%s %s_fn (%s) { %s; };\n" % (
            base.out_type or "void",
            name,
            ",".join(params),
            body
        )
        self.compiled += "const %s %s = { 0, %s_fn };\n" % (base.type, name, name)

    def compile (self):
        return self.compiled

    def __getitem__(self, key):
        return self.items[key]

class NewFnModuleBase:
    def __init__(self, base):
        self.base = base

    def build (self, argument):
        return NewFnModule(self.base, argument)

class ClosureModule:
    def __init__(self, base, argument):
        fn = argument["0"]

        if not hasattr(fn, 'in_types'):
            raise Exception("%r has no in_types" % fn)
        env_tp = fn.in_types[-1]

        self.base = base
        name = compile.gen_name("fnval")

        # Closure internal function
        args = ["_%s"%i for i in range(len(self.base.ins))]
        args.append("*(%s*)env" % env_tp)

        params = ["%s _%d" % (tp, i) for i, tp in enumerate(self.base.ins)]
        params.insert(0, "void *env")

        body = fn.use(args)
        if len(self.base.outs) > 0:
            body = "return " + body

        self.compiled = "%s %s (%s) { %s; };\n" % (base.out_type or "void", name, ",".join(params), body)
        
        self.items = {
            "new": CFunction(self.base.type, name + "_new",
                "(%s value) {" % env_tp +
                "\n\t%s* env = malloc(sizeof(%s));" % (env_tp, env_tp) +
                "\n\t*env = value;" +
                "\n\treturn (%s) { (void*) env, %s };" % (self.base.type, name) +
                "\n}"
            )
        }

    def compile (self):
        return self.compiled

    def __getitem__(self, key):
        item = self.items[key]
        compile.compiled.compile(item)
        return item

class ClosureModuleBase:
    def __init__(self, base):
        self.base = base

    def build (self, argument):
        return ClosureModule(self.base, argument)

class FunctionModule(GenericModule):
    name = "function"
    def init(self, argument):
        self.types = []
        self.items = {}

        self.type = CType(compile.gen_name("fntp"))

        ins, outs = get_fn_params(argument)
        self.ins = ins
        self.outs = outs


        self.items[""] = self.type
        self.items["new"] = NewFnModuleBase(self)

        self.out_type = None
        if len(outs) == 1:
            self.out_type = outs[0]
        elif len(outs) > 1:
            raise Exception("Function values with more than 1 result are not yet supported")

        params = [str(t) for t in ins]
        params.insert(0, "void*")

        self.compiled  = "typedef struct {\n\tvoid* env;\n"
        self.compiled += "\t%s (*fn)(%s);\n" % (self.out_type or "void", ",".join(params))
        self.compiled += "} %s;\n" % self.type

        apply_args = ["#%d"%(i+1) for i in range(len(self.ins))]
        apply_args.insert(0, "#0.env")
        self.items["apply"] = CMacro(self.out_type, "#0.fn(%s)" % ",".join(apply_args))
        self.items["closure"] = ClosureModuleBase(self)

    def init_base (self):
        self.compiled = None

    def compile (self):
        return self.compiled

    def get_key(argument):
        (ins, outs) = get_fn_params(argument)
        return "%s|%s"%(" ".join([str(t.id) for t in ins])," ".join([str(t.id) for t in outs]))

int_tp = CType("int")
float_tp = CType("float")
bool_tp = CType("int")
char_tp = CType("char")
str_tp = CPtrType("char")
buf_tp = CType("auro_buffer")

compile.compiled += "typedef struct { int size; char *bytes; } auro_buffer;"

file_tp = CPtrType("FILE")
mode_tp = CPtrType("char")
ptr_tp = CPtrType("void")

def init_io ():
    compile.includes.add("stdio")

def init_math ():
    compile.includes.add("math")

def init_mem ():
    compile.includes.add("stdlib")

class CharAt:
    def __init__(self):
        self.out_types = [char_tp, int_tp]
        self.out_type = None
    def compile(self):
        if self.out_type == None:
            self.out_type = compile.gen_name("charat_t")
        return "typedef struct { char _0; int _1; } %s;" % self.out_type
    def use(self, args):
        return "{ %s[%s], %s+1 }" % (args[0], args[1], args[1])

class ExecFn (CFunction):
    def __init__(self):
        CFunction.__init__(self, None, "auro_exec", "(char* cmd) { return { 1, \"\" }; }")
        self.out_types = [int_tp, str_tp]
    def compile(self):
        if self.out_type == None:
            self.out_type = compile.gen_name("exec_t")

        tbody = "typedef struct { int _0; char* _1; } %s;\n" % self.out_type
        fbody = "%s %s (char* cmd) { return (%s) { _0: 1, _1: \"\" }; }" % (self.out_type, self.name, self.out_type)

        return tbody + "\n" + fbody

modules = {
    "auro\x1fbool": CModule({
        "bool": bool_tp,
        "true": CMacro(bool_tp, "1"),
        "false": CMacro(bool_tp, "0"),
        "not": CMacro(bool_tp, "!#0"),
    }),
    "auro\x1fint": CModule({
        "int": int_tp,
        "add": CMacro(int_tp, "#0 + #1"),
        "sub": CMacro(int_tp, "#0 - #1"),
        "mul": CMacro(int_tp, "#0 * #1"),
        "div": CMacro(int_tp, "#0 / #1"),
        "eq": CMacro(int_tp, "#0 == #1"),
        "ne": CMacro(int_tp, "#0 != #1"),
        "lt": CMacro(int_tp, "#0 < #1"),
        "gt": CMacro(int_tp, "#0 > #1"),
        "le": CMacro(int_tp, "#0 <= #1"),
        "ge": CMacro(int_tp, "#0 >= #1"),
    }),
    "auro\x1ffloat": CModule({
        "float": float_tp,
        "add": CMacro(float_tp, "#0 + #1"),
        "sub": CMacro(float_tp, "#0 - #1"),
        "mul": CMacro(float_tp, "#0 * #1"),
        "div": CMacro(float_tp, "#0 / #1"),
        "eq": CMacro(float_tp, "#0 == #1"),
        "ne": CMacro(float_tp, "#0 != #1"),
        "lt": CMacro(float_tp, "#0 < #1"),
        "gt": CMacro(float_tp, "#0 > #1"),
        "le": CMacro(float_tp, "#0 <= #1"),
        "ge": CMacro(float_tp, "#0 >= #1"),
        "itof": CMacro(float_tp, "(float)#0"),
        "ftoi": CMacro(int_tp, "(int)#0"),
        "decimal": CFunction(float_tp, "auro_float_decimal",
            "(int mag, int exp) {" +
            "\n\tfloat f = mag;" +
            "\n\twhile (exp < 0) { f /= 10; exp++; }" +
            "\n\twhile (exp > 0) { f *= 10; exp--; }" +
            "\n\treturn f;" +
            "\n}"
        ),
        "infinity": CMacro(float_tp, "1.0/0.0"),
        "nan": CMacro(float_tp, "0.0/0.0"),
    }),
    "auro\x1fint\x1fbit": CModule({
        "shr": CMacro(int_tp, "#0 >> #1"),
        "shl": CMacro(int_tp, "#0 << #1"),
        "and": CMacro(int_tp, "#0 & #1"),
        "or": CMacro(int_tp, "#0 | #1"),
        "xor": CMacro(int_tp, "#0 ^ #1"),
        "not": CMacro(int_tp, "~#0"),
    }),
    "auro\x1fstring": CModule({
        "string": str_tp,
        "char": char_tp,
        "new": CFunction(str_tp, "auro_string_new",
            "(auro_buffer buffer) {" +
            "\n\tint len = buffer.size;" +
            "\n\tchar *str = malloc(len+1);" +
            "\n\tmemcpy(str, buffer.bytes, len);" +
            "\n\tstr[len] = 0;" +
            "\n\treturn str;" +
            "\n}",
        ),
        "tobuffer": CFunction(buf_tp, "auro_string_tobuf",
            "(char *str) {" +
            "\n\tint len = strlen(str);" +
            "\n\tchar *bytes = malloc(len);" +
            "\n\tmemcpy(bytes, str, len);" +
            "\n\treturn (auro_buffer){ len, bytes };" +
            "\n}"
        ),
        "concat": CFunction(str_tp, "auro_string_concat",
            "(char *a, char *b) {" +
            "\n\tint al = strlen(a), bl = strlen(b);" +
            "\n\tchar *c = malloc(al+bl+1);" +
            "\n\tc[0] = 0; strcat(c, a); strcat(c, b);" +
            "\n\treturn c;" +
            "\n}"
        ),
        "itos": CFunction(str_tp, "auro_itos",
            "(int n) {" +
            # log10(2^64) = 19.26
            "\n\tchar *str = malloc(21);" +
            "\n\tstr[0] = 0;" +
            "\n\tsprintf(str, \"%d\", n);" +
            "\n\treturn str;" +
            "\n}"
        ),
        "ftos": CFunction(str_tp, "auro_ftos",
            "(float n) {" +
            "\n\tchar *str = malloc(60);" +
            "\n\tstr[0] = 0;" +
            "\n\tsprintf(str, \"%f\", n);" +
            "\n\treturn str;" +
            "\n}"
        ),
        "eq": CMacro(bool_tp, "strcmp(#0, #1) == 0"),
        "length": CMacro(int_tp, "strlen(#0)"),
        "charat": CharAt(),
        "codeof": CMacro(int_tp, "#0"),
        "newchar": CMacro(char_tp, "#0"),
        "add": CFunction(str_tp, "auro_string_add",
            "(char *a, char b) {" +
            "\n\tchar bb[2] = { b, 0 };" +
            "\n\tchar *c = malloc(strlen(a)+2);" +
            "\n\tstrcpy(c, a); strcat(c, bb);" +
            "\n\treturn c;" +
            "\n}"
        ),
        "slice": CFunction(str_tp, "auro_string_slice",
            "(char *s, int a, int b) {" +
            "\n\tchar *r = malloc(b-a+1);" +
            "\n\tmemcpy(r, s+a, b-a);" +
            "\n\tr[a-b] = 0;" +
            "\n\treturn r;" +
            "\n}"
        ),
    }),
    "auro\x1fbuffer": CModule({
        "buffer": buf_tp,
        "new": CFunction(buf_tp, "auro_buffer_new",
            "(int size) {" +
            "\n\tchar *bytes = calloc(1, size);" +
            "\n\treturn (auro_buffer){ size, bytes };" +
            "\n}"
        ),
        "size": CMacro(int_tp, "#0.size"),
        "get": CMacro(int_tp, "#0.bytes[#1]"),
        "set": CMacro(None, "#0.bytes[#1] = #2"),
    }),
    "auro\x1fsystem": CModule({
        "println": CFunction(None, "auro_println",
            "(char *s) { printf(\"%s\\n\", s); }"
        ),
        "argc": CMacro(int_tp, "argc"),
        "argv": CMacro(str_tp, "argv[#0]"),
        "exit": CMacro(None, "exit(#0)"),
        "error": CMacro(None, "{ printf(\"%s\", #0); exit(1); }"),
        "exec": ExecFn(),
    }),
    "auro\x1fio": CModule({
        "file": file_tp,
        "mode": mode_tp,
        "r": CMacro(mode_tp, "\"rb\""),
        "w": CMacro(mode_tp, "\"wb\""),
        "a": CMacro(mode_tp, "\"ab\""),
        #"open": CMacro(file_tp, "fopen(#0, #1)"),
        "open": CFunction(file_tp, "auro_open_file",
            "(char *filename, char *mode) {" +
            "\n\tFILE* file = fopen(filename, mode);" +
            "\n\tif (!file) { printf(\"Could not open %s\", filename); exit(1); }" +
            "\n\treturn file;" +
            "\n}"
        ),
        "close": CMacro(None, "fclose(#0)"),
        "read": CFunction(buf_tp, "auro_read_file",
            "(FILE *f, int size) {" +
            "\n\tchar* bytes = malloc(size);" +
            "\n\tint redd = fread(bytes, 1, size, f);" +
            "\n\treturn (auro_buffer){ size, bytes };" +
            "\n}",
        ),
        "write": CFunction(None, "auro_write_file",
            "(FILE *f, auro_buffer buf) {" +
            "\n\tint size = buf.size;" +
            "\n\tint written = fwrite(buf.bytes, 1, size, f);" +
            "\n\tif (written != size) { printf(\"Could not write file\"); exit(1); }" +
            "\n}"
        ),
        "eof": CMacro(bool_tp, "feof(#0)"),
    }, init_io),
    "auro\x1fmath": CModule({
        "pi": CMacro(float_tp, "3.141592653589793238462643"),
        "e": CMacro(float_tp, "2.1828"),
        "sqrt2": CMacro(float_tp, "1.4"),

        "abs": CMacro(float_tp, "fabs(#0)"),
        "ceil": CMacro(float_tp, "ceil(#0)"),
        "floor": CMacro(float_tp, "floor(#0)"),
        "round": CMacro(float_tp, "floor(#0+0.5)-0.5"),
        "trunc": CMacro(float_tp, "#0-floor(#0)"),

        "ln": CMacro(float_tp, "log(#0)"),
        "exp": CMacro(float_tp, "exp(#0)"),
        "sqrt": CMacro(float_tp, "sqrt(#0)"),
        #"cbrt": CMacro(float_tp, "cbrt(#0)"),

        "pow": CMacro(float_tp, "pow(#0, #1)"),
        "log": CMacro(float_tp, "log(#0)/log(#1)"),
        "mod": CMacro(float_tp, "fmod(#0, #1)"),

        "sin": CMacro(float_tp, "sin(#0)"),
        "cos": CMacro(float_tp, "cos(#0)"),
        "tan": CMacro(float_tp, "tan(#0)"),
        "asin": CMacro(float_tp, "asin(#0)"),
        "acos": CMacro(float_tp, "acos(#0)"),
        "atan": CMacro(float_tp, "atan(#0)"),
        "atan2": CMacro(float_tp, "atan2(#0,#1)"),
        "sin": CMacro(float_tp, "sinh(#0)"),
        "cos": CMacro(float_tp, "cosh(#0)"),
        "tan": CMacro(float_tp, "tanh(#0)"),
    }, init_math),
    "auro\x1fmachine\x1fmem": CModule({
        "pointer": ptr_tp,

        "null": CMacro(ptr_tp, "0"),
        "alloc": CMacro(ptr_tp, "calloc(#0, 1)"),
        "read": CMacro(int_tp, "*((char*) #0)"),
        "write": CMacro(None, "*((char*) #0)=#1"),
        "offset": CMacro(ptr_tp, "((char*)#0)+#1"),
        "size": CMacro(int_tp, "sizeof(size_t)"),

        "readany": CMacro(any_tp, "*((auro_any*)#0)"),
        "allocany": CFunction(ptr_tp, "auro_allocany",
            "(auro_any a) {" +
            "\n\tauro_any *p = malloc(sizeof(a));" +
            "\n\t*p = a;" +
            "\n\treturn p;" +
            "\n}"
        ),

        "read\x1dpointer": CMacro(ptr_tp, "*((void**)#0)"),
        "write\x1dpointer": CMacro(None, "*((void**)#0)=#1"),

        "read\x1df64": CMacro(float_tp, "(float)*((double*)#0)"),
        "write\x1df64": CMacro(None, "*((double*)#0)=(double)#1"),
    }, init_mem),
    "auro\x1frecord": RecordModule(),
    "auro\x1fany": AnyModule(),
    "auro\x1ftypeshell": TypeShell(),
    "auro\x1fnull": NullModule(),
    "auro\x1farray": ArrayModule(),
    "auro\x1ffunction": FunctionModule(),
}

import ffi

def init_prim ():
    compile.includes.add("stdint")

modules["auro\x1fffi"] = ffi.FfiModule()
modules["auro\x1fffi\x1ffunction"] = ffi.FfiFunctionModule()
modules["auro\x1fmachine\x1fprimitive"] = CModule(ffi.prim_items, init_prim)

for name, mod in modules.items():
    if isinstance(mod, CModule):
        mod.name = name.replace("\x1f", ".")
        for iname, item in mod.items.items():
            item.nugget = nugget.Importable(name, iname)
