
from sys import argv
import os
import tempfile
import subprocess

import parse
import compile

def help (code=0):
    print("usage: %s OPTIONS <module>" % argv[0])
    print("\t--dir <dir> add dir to search <path>")
    print("\t--out <out> output binary name, defaults to \"out\"")
    print("\t--release   makes the binary faster, but the compilation slower")
    quit(code)

dirs = []
out_filename = "out"
module = None
release_mode = False
cout_filename = None

arg_i = 1
while arg_i < len(argv):
    arg = argv[arg_i]
    if arg == "--help" or arg == "-h":
        help()
    elif arg == "--dir":
        arg_i += 1
        dirs.append(argv[arg_i])
    elif arg == "--cout":
        arg_i += 1
        cout_filename = argv[arg_i]
    elif arg == "--out":
        arg_i += 1
        out_filename = argv[arg_i]
    elif arg == "--release":
        release_mode = True
    elif arg[0] == "-":
        print("Unknown option: " + arg)
        help(1)
    else:
        if module:
            print("Only one module allowed")
            quit(1)
        module = arg
    arg_i += 1

if module == None: help(1)

if len(dirs) == 0:
    dirs = ["."]

home = os.getenv("HOME")
if home:
    aurodir = home + "/.auro/modules"
    if os.path.isdir(aurodir):
        dirs.insert(0, aurodir)

def find_module_file (modname):
    modname = modname.replace("\x1f", ".")
    for d in reversed(dirs):
        path = "%s/%s" % (d, modname)
        if os.path.exists(path):
            return path
    return None

cache = {}

def get_module (modname):
    try:
        return cache[modname]
    except KeyError:
        pass

    filename = find_module_file(modname)
    if not filename: return None
    
    file = open(filename, "rb")
    parsed = parse.parse(file, filename)
    parsed.filename = filename
    mod = compile.ParsedModule(modname, parsed)
    cache[modname] = mod
    return mod

compile.set_module_loader(get_module)
compiled = compile.compile(module)

source = tempfile.NamedTemporaryFile(mode="w+")
source.write(compiled.output)
source.flush()

# -x c: language c
# -lm: link math
# -O3: maximum speed optimization
args = ["gcc", "-x", "c", source.name, "-lm", "-o", out_filename]

if release_mode:
    args.append("-O3")

args.extend(compile.flags)

if cout_filename != None:
    subprocess.run(["cp", source.name, cout_filename])

print("CC: " + " ".join(args))
subprocess.run(args, check=True)
