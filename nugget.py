from ctypes import *

libc = CDLL("../nugget/libnugget.so")

class ITEM(Structure):
    _fields_ = [("kind", c_int), ("item", c_void_p)]

machineNew = libc.nuggetMachineNew
machineNew.argtypes = [c_void_p, c_void_p]
machineNew.restype = c_void_p

moduleBuiltin = libc.nuggetModuleBuiltin
moduleBuiltin.argtypes = [c_char_p]
moduleBuiltin.restype = c_void_p

machineGetModule = libc.nuggetMachineGetModule
machineGetModule.argtypes = [c_void_p, c_char_p]
machineGetModule.restype = c_void_p

moduleGetItem = libc.nuggetModuleGetItem
moduleGetItem.argtypes = [c_void_p, c_char_p]
moduleGetItem.restype = ITEM

functionGetInput = libc.nuggetFunctionGetInput
functionGetInput.restype = c_void_p

functionGetInputCount = libc.nuggetFunctionGetInputCount
functionGetInput.restype = c_int

functionGetOutput = libc.nuggetFunctionGetOutput
functionGetOutput.restype = c_void_p

functionGetOutputCount = libc.nuggetFunctionGetOutputCount
functionGetOutputCount.restype = c_int

functionArgsNew = libc.nuggetFunctionArgsNew
functionArgsNew.restype = c_void_p

functionArgsWriteInput = libc.nuggetFunctionArgsWriteInput
functionArgsWriteInput.argtypes = [c_void_p, c_int, c_void_p]

functionArgsReadOutput = libc.nuggetFunctionArgsReadOutput
functionArgsReadOutput.argtypes = [c_void_p, c_int, c_void_p]

functionCall = libc.nuggetFunctionCall
functionCall.argTypes = [c_void_p, c_void_p, c_void_p]

typeInt = libc.nuggetTypeInt
typeInt.restype = c_void_p

typeFloat = libc.nuggetTypeFloat
typeFloat.restype = c_void_p

typeString = libc.nuggetTypeString
typeString.restype = c_void_p

typeGetName = libc.nuggetTypeGetId
typeGetName.restype = c_char_p

typeGetId = libc.nuggetTypeGetId
typeGetId.argtypes = [c_void_p]
typeGetId.restype = c_int

compiledModuleGetFunction = libc.nuggetCompiledModuleGetFunction
compiledModuleGetFunction.restype = c_void_p

moduleDescLoad = libc.nuggetModuleDescLoad
moduleDescLoad.restype = c_void_p

readFile = libc.nuggetReadFile
readFile.restype = c_void_p

nuggetError = libc.nuggetError
nuggetError.restype = c_char_p

nuggetMachinePrintErrors = libc.nuggetMachinePrintErrors


MODLOADERFUNC = CFUNCTYPE(c_void_p, c_void_p, c_char_p)

def py_mod_loader (machine, c_name):
    print("Loading module: %s" % c_name)
    return moduleBuiltin(c_name)

c_mod_loader = MODLOADERFUNC(py_mod_loader)

machine = c_void_p(machineNew(c_mod_loader, 0))

class Function:
    def __init__(self, c_ptr):
        if not c_ptr:
            raise Exception("Tried to create a nugget function with a null pointer")
        self.f_ptr = c_ptr

    def call(self, args):
        incount = functionGetInputCount(self.f_ptr)

        if len(args) != incount:
            raise Exception("Wrong argument count. Passed %d but function expects %d" % (len(args), incount))

        cargs = c_void_p(functionArgsNew(self.f_ptr))

        for i, arg in enumerate(args):
            tptr = c_void_p(functionGetInput(self.f_ptr, i))
            tid = typeGetId(tptr)

            carg = None
            if tid == typeGetId(typeInt()):
                carg = c_int(arg)
            elif tid == typeGetId(typeString()):
                carg = create_string_buffer(bytes(arg, "utf8"))
            else:
                tname = typeGetName(tptr).value.decode("utf8")
                raise Exception("Argument type not supported: '%s' (tid: %d)" % (tname, tid))

            functionArgsWriteInput(cargs, c_int(i), byref(carg))

        functionCall(machine, self.f_ptr, cargs)

        rets = []
        outcount = functionGetOutputCount(self.f_ptr)
        for i in range(0, outcount):
            tptr = c_void_p(functionGetOutput(self.f_ptr, i))
            tid = typeGetId(tptr)

            cval = None
            if tid == typeGetId(typeInt()):
                cval = c_int(0)
            elif tid == typeGetId(typeString()):
                cval = c_char_p(0)
            elif tid == typeGetId(typeFloat()):
                cval = c_float(0.0)
            else:
                raise Exception("Result type not supported: '%s' (tid: %d)" % (typeGetName(tptr), tid))

            functionArgsReadOutput(cargs, c_int(i), byref(cval))

            pval = None
            if tid == typeGetId(typeString()):
                pval = cval.value.decode("utf8")
            if tid == typeGetId(typeInt()):
                pval = cval.value
            if tid == typeGetId(typeFloat()):
                pval = cval.value

            rets.append(pval)

        return rets

class Importable:
    def __init__(self, modname, itemname):
        self.modname = modname
        self.itemname = itemname
        self.loaded = None

    def call (self, args):
        if self.loaded == None:
            modname = create_string_buffer(bytes(self.modname, "utf8"))
            nugget_mod = machineGetModule(machine, modname)
            if not nugget_mod:
                raise Exception("Could not load nugget module: %s", self.modname)

            itemname = create_string_buffer(bytes(self.itemname, "utf8"))
            item = moduleGetItem(nugget_mod, itemname)

            if item.kind != 3:
                raise Exception("Nugget item '%s' in %s is not a function" % (self.itemname, self.modname))

            self.loaded = Function(c_void_p(item.item))

        return self.loaded.call(args)

class NuggetModule:
    def __init__(self, ptr):
        self.ptr = ptr

    def from_file (filename):
        c_fname = create_string_buffer(bytes(filename, "utf8"))
        desc = readFile(c_fname)

        if not desc:
            c_msg = nuggetError()
            msg = c_msg.decode("utf8")
            raise Exception("An error ocurreed trying to read %s:\n%s" % (filename, msg))

        ptr = moduleDescLoad(c_void_p(desc), machine, c_fname)
        return NuggetModule(c_void_p(ptr))


class InternalFunction:
    def __init__(self, module, index):
        self.module = module
        self.index = index
        self.loaded = None

    def call(self, args):
        if self.loaded == None:
            ptr = compiledModuleGetFunction(self.module.ptr, self.index)
            if not ptr:
                nuggetMachinePrintErrors(machine)
                raise Exception("Could not get auro function %d in module" % self.index)
            self.loaded = Function(c_void_p(ptr))

        return self.loaded.call(args)