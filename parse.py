
def read_byte ():
    bin = infile.read(1)
    n = int.from_bytes(bin, "big")
    return n

def read_int ():
    n = 0
    byte = read_byte()
    while (byte & 0x80) > 0:
        n = (n << 7) | (byte & 0x7f)
        byte = read_byte()
    n = (n << 7) | (byte & 0x7f)
    return n

def read_str ():
    size = read_int()
    bytes = infile.read(size)
    return bytes.decode('utf-8')



###  Header  ###

def parse_header():
    header = ""
    byte = infile.read(1)
    while byte != b"\0":
        header += byte.decode('ascii')
        if len(header) > 30:
            raise Exception("Header too large: %r" % header)
        byte = infile.read(1)

    if header != "Auro 0.6":
        raise Exception("Invalid header: %r" % header)
    return header



###  Modules  ###

class UnknwonModule:
    pass

class ImportModule:
    def __init__(self, name):
        self.name = name
    def __repr__(self):
        return "ImportModule(%r)" % self.name

class DefineItem:
    def __init__(self, kind, index):
        self.kind = kind
        self.index = index
    def __repr__(self):
        return "Item(%s, %s)" % (self.kind, self.index)

class DefineModule:
    def __init__(self, items):
        self.items = items
    def __repr__(self):
        return "DefineModule(%r)" % self.items

class UseModule:
    def __init__(self, base, name):
        self.base = base
        self.name = name

class BuildModule:
    def __init__(self, base, argument):
        self.base = base
        self.argument = argument

def parse_modules():
    module_count = read_int()
    modules = []

    for i in range(0, module_count):
        kind = read_int()
        if kind == 0:
            module = UnknownModule()
        elif kind == 1:
            module = ImportModule(read_str())
        elif kind == 2:
            items = {}
            item_count = read_int()
            for j in range(0, item_count):
                item = DefineItem(
                        kind = read_int(),
                        index = read_int()
                        )
                name = read_str()
                items[name] = item
            module = DefineModule(items)
        elif kind == 3:
            module = UseModule(
                    base = read_int(),
                    name = read_str(),
                    )
        elif kind == 4:
            module = BuildModule(
                    base = read_int(),
                    argument = read_int()
                    )
        else:
            raise Exception("Unknown module kind: " + str(kind))
        modules.append(module)
    return modules



###  Types  ###

class Type:
    def __init__(self, module, name):
        self.module = module
        self.name = name
    def __repr__(self):
        return "Type(module=" + repr(self.module) + ",name=" + repr(self.name) + ")"

def parse_types():
    types = []
    type_count = read_int()

    for i in range(0, type_count):
        types.append(Type(
            module = read_int()-1,
            name = read_str()
            ))
    return types



###  Functions  ###

class Function:
    def __init__(self, ins, outs):
        self.ins = ins
        self.outs = outs

class UnknownFunction(Function):
    def __repr__(self):
        return "UnknownFunction"

class ImportFunction(Function):
    def __init__(self, ins, outs, module, name):
        Function.__init__(self, ins, outs)
        self.module = module
        self.name = name

    def __repr__(self):
        sig = "ins=%r,outs=%r" % (self.ins, self.outs)
        _def = "module=%r,name=%r" % (self.module, self.name)
        return "ImportFunction(%s,%s)" % (_def, sig)

class CodeFunction(Function):
    def __init__(self, ins, outs):
        Function.__init__(self, ins, outs)
        self.code = []
    def __repr__(self):
        return "CodeFunction(...)"

def parse_functions():
    functions = []
    function_count = read_int()

    for i in range(0, function_count):
        kind = read_int()
        ins = []
        outs = []

        in_count = read_int()
        for j in range(0, in_count):
            ins.append(read_int())

        out_count = read_int()
        for j in range(0, out_count):
            outs.append(read_int())

        if kind == 0:
            function = UnknownFunction(ins, outs)
        elif kind == 1:
            function = CodeFunction(ins, outs)
        elif kind > 1:
            name = read_str()
            function = ImportFunction(ins, outs, kind-2, name)

        functions.append(function)
    return functions

###  Constants  ###

class Constant(Function):
    def __init__(self):
        Function.__init__(self, ins=[], outs=[None])

class IntConstant(Constant):
    def __init__(self, value):
        Constant.__init__(self)
        self.value = value
    def __repr__(self):
        return "IntConstant(%s)" % self.value

class BufferConstant(Constant):
    def __init__(self, bytes):
        Constant.__init__(self)
        self.bytes = bytes
    def __repr__(self):
        return "BufferConstant(%r)" % self.bytes

class CallConstantCall:
    def __init__(self, fn_index, args):
        self.fn_index = fn_index
        self.args = args

class CallConstant(Constant):
    def __init__(self, call, index):
        Constant.__init__(self)
        self.call = call
        self.index = index
    def __repr__(self):
        return "CallConstant(fn=%r,args=%r,index=%r)" % (self.call.fn_index, self.call.args, self.index)

def parse_constants(functions):
    function_count = len(functions)

    constant_count = read_int()

    for i in range(0, constant_count):
        kind = read_int()
        if kind == 1:
            functions.append(IntConstant(read_int()))
        elif kind == 2:
            buf_size = read_int()
            bytes = infile.read(buf_size)
            functions.append(BufferConstant(bytes))
        elif kind < 16:
            raise Exception("Unknown constant kind", kind)
        elif kind >= 16:
            fn_index = kind - 16
            if fn_index > len(functions) and fn_index < (function_count + constant_count):
                in_count = 0
                out_count = 1
            else:
                fn = functions[fn_index]
                in_count = len(fn.ins)
                out_count = len(fn.outs)
            ins = []
            for j in range(0, in_count):
                ins.append(read_int())
            call = CallConstantCall(fn_index, ins)
            for j in range(0, out_count):
                functions.append(CallConstant(call, j))



###  Code  ###

class Inst:
    pass

class EndInst:
    def __init__(self, args=[]):
        self.args = args
    def __repr__(self):
        return "EndInst(args=%r)" % self.args

class HltInst:
    pass

class VarInst:
    def __init__(self, out):
        self.out = out

class DupInst:
    def __init__(self, a, out):
        self.a = a
        self.out = out

class SetInst:
    def __init__(self, b, a):
        self.a = a
        self.b = b

class JmpInst:
    def __init__(self, i):
        self.i = i

class JifInst:
    def __init__(self, i, a):
        self.a = a
        self.i = i

class NifInst:
    def __init__(self, i, a):
        self.a = a
        self.i = i

class CallInst:
    def __init__(self, fn, ins, outs):
        self.fn = fn
        self.ins = ins
        self.outs = outs
    def __repr__(self):
        return "CallInst(fn=%r,ins=%r,outs=%r)" % (self.fn, self.ins, self.outs)

def parse_code(functions):
    for fn in functions:
        if isinstance(fn, CodeFunction):
            code_len = read_int()
            # Count the arguments to the current function
            reg_count = len(fn.ins)
            for i in range(0, code_len):
                kind = read_int()
                if kind == 0:
                    inst = EndInst([read_int() for j in range(0, len(fn.outs))])
                elif kind == 1:
                    inst = HltInst()
                elif kind == 2:
                    inst = VarInst(out = reg_count)
                    reg_count += 1
                elif kind == 3:
                    inst = DupInst(a = read_int(), out = reg_count)
                    reg_count += 1
                elif kind == 4:
                    inst = SetInst(b = read_int(), a = read_int())
                elif kind == 5:
                    inst = JmpInst(read_int())
                elif kind == 6:
                    inst = JifInst(i = read_int(), a = read_int())
                elif kind == 7:
                    inst = NifInst(i = read_int(), a = read_int())
                elif kind < 16:
                    raise Exception("Unknown instruction " + str(kind))
                elif kind >= 16:
                    fn_index = kind - 16
                    call_fn = functions[fn_index]
                    in_count = len(call_fn.ins)
                    out_count = len(call_fn.outs)
                    ins = []
                    outs = []

                    for j in range(0, in_count):
                        ins.append(read_int())
                    for j in range(0, out_count):
                        outs.append(reg_count + j)
                    reg_count += out_count

                    inst = CallInst(fn_index, ins, outs)
                fn.code.append(inst)
            fn.reg_count = reg_count

class MetaNode:
    def __init__(self, value):
        self.value = value

    def __getitem__(self, key):
        if type(self.value) != list:
            return None

        if type(key) == int:
            return self.value[key]

        for val in self.value:
            inner = val.value
            if type(inner) == list and inner[0].value == key:
                return val

    def __str__(self):
        if type(self.value) == list:
            return "(%s)" % ", ".join([str(n) for n in self.value])
        return str(self.value)

    def __iter__(self):
        return self.value.__iter__()

    def __eq__(self, other):
        if type(other) == MetaNode:
            return self.value == other.value
        return self.value == other

    def __hash__(self):
        return hash(str(self))

    def __contains__(self, item):
        if self[item]: return True
        return item in self.value

def parse_metanode ():
    n = read_int()
    if (n & 1) == 1:
        return MetaNode(n >> 1)
    elif (n & 2) == 2:
        bytes = infile.read(n >> 2)
        str = bytes.decode('utf-8')
        return MetaNode(str)
    else:
        return MetaNode([parse_metanode() for _ in range(0, n >> 2)])

class Parsed:
    pass

def parse (_infile, filename):
    global infile
    infile = _infile
    parsed = Parsed()
    parsed.header = parse_header()
    parsed.modules = parse_modules()
    parsed.types = parse_types()
    parsed.functions = parse_functions()
    parse_constants(parsed.functions)
    parse_code(parsed.functions)
    parsed.metadata = parse_metanode()
    return parsed

if __name__ == "__main__":
    from sys import argv

    if len(argv) != 2:
        print("One argument required")
        quit(1)

    filename = argv[1]

    infile = open(filename, "rb")

    parsed = parse(infile)

    print("Modules")
    for m in parsed.modules:
        print("\t", m)
    print("Types")
    for t in parsed.types:
        print("\t", t)
    print("Functions")
    for f in parsed.functions:
        print("\t", f)
        if isinstance(f, CodeFunction):
            for i in f.code:
                print("\t\t", i)
